#ifndef OBJECT_H_INCLUDED
#define OBJECT_H_INCLUDED
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_gfxPrimitives.h>

#include "main.h"
typedef enum ObjectID
{
    //csak a speci�lis eseteket sorolom itt fel. A t�bbire index szerint lehet hivatkozni.
    //A nevek csak a k�nnyebb �tl�thats�g �rdek�ben szerepelnek
    EMPTY = 0,
    GRASS = 1,
    STONE = 7,
    STONE_WALL1 = 58,
    STONE_WALL2 = 46,
    STONE_BRICK1 = 8,
    STONE_BRICK2 = 9,
    STONE_BRICK3 = 10,
    STONE_BRICK4 = 19,
    STONE_BRICK5 = 20,
    STONE_BRICK6 = 21,
    STONE_BRICK7 = 22,
    STONE_BRICK8 = 31,
    STONE_BRICK9 = 32,
    STONE_BRICK10 = 34,
    BUSH1 = 13,
    BUSH2 = 15,
    BUSH3 = 3,
    TRUNK1 = 4,
    TRUNK2 = 26,
    PUBBLES1 = 30,
    ROCK1 = 25,
    TREE = 5,
    TREE1_BOTTOM_LEFT = 105,
    TREE1_BOTTOM_RIGHT = 106,
    TREE2_BOTTOM_RIGHT = 107,
    TREE2_BOTTOM_LEFT = 108,
    WATER_TOP_LEFT = 37,
    WATER_TOP = 38,
    WATER_TOP_RIGHT = 39,
    WATER_LEFT = 49,
    WATER = 50,
    WATER_RIGHT = 51,
    WATER_BOTTOM_LEFT = 61,
    WATER_BOTTOM = 62,
    WATER_BOTTOM_RIGHT = 63,

} objectId;
typedef enum Type
{
    NOTSOLID, //�t lehet menni rajta, a j�t�kos fel� ker�l
    SOLID, //Nem lehet �tmenni rajta
    WALKABLE //�t lehet menni rajta, a j�t�kos al� ker�l

} Type;

typedef enum Layer
{
    BACK,
    FRONT
} Layer;

typedef struct Object
{
    Position position;
    Rect sheetPos;
    Rect collisionRect;
    objectId id;
    Type type;
    Layer layer;
    objectId bottomid;
} Object;

void createObject(Object *object, objectId id, int x, int y, Type type);
void drawobject(Object object, Position worldpos, SDL_Surface *texture, SDL_Surface *screen);



#endif // OBJECT_H_INCLUDED
