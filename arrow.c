#include <SDL.h>
#include <SDL_image.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_rotozoom.h>
#include "arrow.h"
#include "world.h"

void addArrow(World *world, Position position, Direction direction)
{
    ArrowListaElem *newArrow;

    newArrow = (ArrowListaElem*) malloc(sizeof(ArrowListaElem));

    newArrow->arrows.position = position;
    newArrow->arrows.direction = direction;

    newArrow->kov = world->arrows.elso->kov;
    newArrow->elozo = world->arrows.elso;
    world->arrows.elso->kov->elozo = newArrow;
    world->arrows.elso->kov = newArrow;



}
void removeArrow(ArrowListaElem *arrow)
{
    //"kifuzz�k" a list�b�l

    arrow->elozo->kov =arrow->kov;
    arrow->kov->elozo = arrow->elozo;
    free(arrow);
}
void drawArrows(World *world, SDL_Surface *screen)
{
    ArrowListaElem *mozgo;
    int db = 0;
    mozgo = world->arrows.elso->kov;
    while(mozgo != world->arrows.utolso)
    {
        //Kirajzoljuk az �sszes ijjat;
        SDL_Rect dest = {mozgo->arrows.position.x - world->min.x, mozgo->arrows.position.y - world->min.y, 64, 64};
        SDL_BlitSurface(rotozoomSurface(ArrowIMG, mozgo->arrows.direction*90, 1.0, 0), NULL, screen, &dest);

        #ifdef _SHOWCOLLISIONRECTS
        //Draw Arrow collisionRect
            rectangleColor(screen, mozgo->arrows.collisionRect.x-world->min.x, mozgo->arrows.collisionRect.y-world->min.y, mozgo->arrows.collisionRect.x-world->min.x+mozgo->arrows.collisionRect.w,
                       mozgo->arrows.collisionRect.y-world->min.y+mozgo->arrows.collisionRect.h, 0xff0000ff);
        #endif
        mozgo = mozgo->kov;
        db++;


    }
    #ifdef _SHOWCOLLISIONRECTS
    char text[100];
    sprintf(text, "Arrows: %d", db);
    stringRGBA(screen, 350, 500, text, 255, 255, 255, 255);
    #endif


}
void freeArrowList(ArrowList *lista)
{
    ArrowListaElem *mozgo;

    mozgo = lista->elso->kov;
    while( mozgo != lista->utolso)
    {
        ArrowListaElem *temp = mozgo->kov;
        free(mozgo);
        mozgo = temp;
    }
    //felszabad�tjuk a str�zs�t is
    free(lista->elso);
    free(lista->utolso);
}
void moveArrows(World *world)
{
    ArrowListaElem *mozgo;
    mozgo = world->arrows.elso->kov;
    while(mozgo != world->arrows.utolso)
    {
        if(mozgo->arrows.direction == RIGHT1)
            mozgo->arrows.position.x += 10;
        if(mozgo->arrows.direction == LEFT1)
            mozgo->arrows.position.x -= 10;
        if(mozgo->arrows.direction == UP1)
            mozgo->arrows.position.y -= 10;
        if(mozgo->arrows.direction == DOWN1)
            mozgo->arrows.position.y += 10;

        if(mozgo->arrows.direction == UP1 || mozgo->arrows.direction == DOWN1)
        {
            //directiontol f�ggoen be�llijuk a collisionRect-et
            mozgo->arrows.collisionRect.x = mozgo->arrows.position.x+30;
            mozgo->arrows.collisionRect.y = mozgo->arrows.position.y+16;
            mozgo->arrows.collisionRect.w = 6;
            mozgo->arrows.collisionRect.h = 30;
        }
        else
        {
            mozgo->arrows.collisionRect.x = mozgo->arrows.position.x+16;
            mozgo->arrows.collisionRect.y = mozgo->arrows.position.y+30;
            mozgo->arrows.collisionRect.w = 31;
            mozgo->arrows.collisionRect.h = 5;
        }


        ArrowListaElem *temp = mozgo->kov;
        if(mozgo->arrows.position.x < world->min.x || mozgo->arrows.position.y < world->min.y
           ||mozgo->arrows.position.x > world->min.x +SCREEN_WIDTH|| mozgo->arrows.position.y > world->min.y +SCREEN_HEIGHT)
        {
            //t�r�lj�k a nyilat
            removeArrow(mozgo);
        }
        mozgo = temp;
    }

}
