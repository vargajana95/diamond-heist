#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_gfxPrimitives.h>


typedef enum Direction {
    RIGHT1,
    UP1,
    LEFT1,
    DOWN1

} Direction;



typedef struct Position{
    int x;
    int y;
} Position, Size;
typedef struct Rect{
    int x;
    int y;
    int w;
    int h;
} Rect;

SDL_Surface *DiamondIMG;
SDL_Surface *EnemyIMG;
SDL_Surface *Enemy2IMG;
SDL_Surface *ArrowIMG;

SDL_Surface *screen;


#endif // MAIN_H_INCLUDED
