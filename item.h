#ifndef ITEM_H_INCLUDED
#define ITEM_H_INCLUDED

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_gfxPrimitives.h>
#include "main.h"
typedef enum ItemID {
    DIAMOND,
    NOTHING
} ItemID;
typedef struct Item {
    Position position;
    SDL_Surface *IMG;
    ItemID id;
    Rect collisionRect;
    int currentFrame;
    int numberOfFrames;
} Item;

typedef struct ItemListaElem {
    Item item;
    struct ItemListaElem *elozo, *kov;
} ItemListaElem;
typedef struct ItemList {
    ItemListaElem *elso;
    ItemListaElem *utolso;
} ItemList;

typedef struct World World;


void AddItem(World *world, Position position, int numberOfFrames, ItemID id);
void removeItem(ItemListaElem *item);
void drawItems(World *world, SDL_Surface *screen, Item item);
void checkForItemPickups(World *world);
int updateItemFrames(Item *item, int repeat);
void generateDiamonds(World *world);
void freeItemList(ItemList *lista);


#endif // ITEM_H_INCLUDED
