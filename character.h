#ifndef CHARACTER_H_INCLUDED
#define CHARACTER_H_INCLUDED

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_gfxPrimitives.h>
#include "main.h"
#include "item.h"

typedef enum moveId {
    NONE,
    THRUST_UP,
    THRUST_LEFT,
    THRUST_DOWN,
    THRUST_RIGHT,
    UP,
    LEFT,
    DOWN,
    RIGHT,
    SHOOT_UP,
    SHOOT_LEFT,
    SHOOT_DOWN,
    SHOOT_RIGHT,
    DIE,
    HOLD_SHOOT_UP,
    HOLD_SHOOT_LEFT,
    HOLD_SHOOT_DOWN,
    HOLD_SHOOT_RIGHT,
    LET_SHOOT_UP,
    LET_SHOOT_LEFT,
    LET_SHOOT_DOWN,
    LET_SHOOT_RIGHT,


    MAX_MOVENUM

} moveId;

typedef struct Path {
    int col, row;
    struct Path *next;
} Path;

typedef struct Attacking {
    int spear;
    int bow;
} Attacking;
typedef struct moveType
{
    int numberOfFrames;
    int startX;
    int startY;
} moveType;
typedef enum EnemyType {
    SILVER,
    GOLD
} EnemyType;

typedef struct Character {
    Position position;
    moveType movetype[MAX_MOVENUM];
    moveId moveid;
    double speed;
    int currentFrame;
    Rect collisionRect;
    SDL_Surface *IMG;
    Direction direction;
    int diamonds;
    Attacking isAttacking;
    Rect hitBox;
    int health;
    int maxHealth;
    int stamina;
    int isDead;
    int isFollowing;
    Position destination; //Az ellenségek ehhez a ponthoz mozognak
    EnemyType type;
    ItemID itemDrop;    //Ezt az itemet dobja el halal utans
} Character;

typedef struct EnemyListaElem {
    Character Enemy;
    struct EnemyListaElem *elozo, *kov;
} EnemyListaElem;
typedef struct EnemyList {
    EnemyListaElem *elso;
    EnemyListaElem *utolso;
} EnemyList;

typedef struct World World;




void AddEnemy(World *world, Position position, EnemyType type, moveId moveid, int currentFrame,
                     int health, int maxHealth, int isDead, Position destination, int isFollowing);
void removeEnemy(EnemyListaElem *enemy);
void moveEnemy(World *world, Character *enemy, Character *player);
void characterDraw(World *world, SDL_Surface *screen, Character character);
int updateFrames(Character *character, int repeat);
void updateCharacters(World *world);
void freeEnemyList(EnemyList *lista);
void setEnemyDestination(World *world, Character *enemy);
void generateEnemies(World *world);
void AddPlayer(World *world, Position position, moveId moveid, int currentFrame,
                     int health, int isDead, int diamonds, double speed, int stamina);
int isNear(Position p1, Position p2, int distance);
void drawEnemyHealthbar(World *world, SDL_Surface *screen, Character character);


#endif // CHARACTER_H_INCLUDED
