#include "character.h"
#include "world.h"
#include "main.h"


moveType movetype[MAX_MOVENUM] =
{
    {1, 0, 10}, //NONE
    {8, 0, 4}, //THRUST_UP
    {8, 0, 5}, //THRUST_LEFT
    {8, 0, 6}, //THRUST_DOWN
    {8, 0, 7}, //THRUST_RIGHT
    {9, 0, 8}, //UP
    {9, 0, 9}, //LEFT
    {9, 0, 10}, //DOWN
    {9, 0, 11}, //RIGHT
    {9, 0, 16}, //SHOOT_UP
    {9, 0, 17}, //SHOOT_LEFT
    {9, 0, 18}, //SHOOT_DOWN
    {9, 0, 19}, //SHOOT_RIGHT
    {6, 0, 20}, //DIE
    {1, 8, 16}, //HOLD_SHOOT_UP
    {1, 8, 17}, //HOLD_SHOOT_LEFT
    {1, 8, 18}, //HOLD_SHOOT_DOWN
    {1, 8, 19}, //HOLD_SHOOT_RIGHT
    {4, 8, 16}, //LET_SHOOT_UP
    {4, 8, 17}, //LET_SHOOT_UP
    {4, 8, 18}, //LET_SHOOT_UP
    {4, 8, 19} //LET_SHOOT_UP
};
void AddPlayer(World *world, Position position, moveId moveid, int currentFrame,
                     int health, int isDead, int diamonds, double speed, int stamina)
{
    int i;
    for(i = 0; i <MAX_MOVENUM; i++)
        world->player.movetype[i] = movetype[i];

    world->player.diamonds = diamonds;
    world->player.health = health;
    world->player.isDead = isDead;
    world->player.position = position;
    world->player.currentFrame = currentFrame;
    world->player.moveid = moveid;
    world->player.speed = speed;
    world->player.direction = DOWN1;
    world->player.maxHealth = MAX_HEALTH;
    world->player.stamina = stamina;
}

void AddEnemy(World *world, Position position, EnemyType type, moveId moveid, int currentFrame,
                     int health, int maxHealth, int isDead, Position destination, int isFollowing)
{
    EnemyListaElem *newEnemy;
    int i;
    newEnemy = (EnemyListaElem*) malloc(sizeof(EnemyListaElem));
    for(i = 0; i <MAX_MOVENUM; i++)
        newEnemy->Enemy.movetype[i] = movetype[i];

    newEnemy->Enemy.currentFrame = currentFrame;
    newEnemy->Enemy.position = position;
    newEnemy->Enemy.destination = destination;
    if(type == GOLD)
    {
        newEnemy->Enemy.IMG = Enemy2IMG;
        newEnemy->Enemy.speed = 5;
        newEnemy->Enemy.itemDrop = DIAMOND;
        newEnemy->Enemy.type = type;
        world->numberOfDiamonds++;
    }
    else
    {
        newEnemy->Enemy.IMG = EnemyIMG;
        newEnemy->Enemy.speed = 4;
        newEnemy->Enemy.itemDrop = NOTHING;
        newEnemy->Enemy.type = type;
    }
    newEnemy->Enemy.health = health;
    newEnemy->Enemy.isDead = isDead;
    newEnemy->Enemy.moveid = moveid;
    newEnemy->Enemy.isFollowing = isFollowing;
    newEnemy->Enemy.maxHealth = maxHealth;

    //Betessz�k a lista elej�re
    newEnemy->kov = world->enemies.elso->kov;
    newEnemy->elozo = world->enemies.elso;
    world->enemies.elso->kov->elozo = newEnemy;
    world->enemies.elso->kov = newEnemy;

}
void freeEnemyList(EnemyList *lista)
{
    EnemyListaElem *mozgo;

    mozgo = lista->elso->kov;
    while( mozgo != lista->utolso)
    {
        EnemyListaElem *temp = mozgo->kov;
        free(mozgo);
        mozgo = temp;
    }
    //felszabad�tjuk a str�zs�t is
    free(lista->elso);
    free(lista->utolso);
}
void removeEnemy(EnemyListaElem *enemy)
{
    //"kifuzz�k" a list�b�l

    enemy->elozo->kov =enemy->kov;
    enemy->kov->elozo = enemy->elozo;
    //SDL_FreeSurface(enemy->Enemy.IMG); //Nem kellen mindig ezt felszabad�tani, egy glob�lis v�ltoz�ba kellene tenni a k�pet.
    free(enemy);
}
void AddToPath(Path *path, int row, int col)
{
    Path *iter;
    Path *uj;

    uj = (Path*) malloc(sizeof(Path));
    uj->col = col;
    uj->row = row;
    uj->next = NULL;

    for(iter = path; iter->next != NULL; iter=iter->next)
    ;
        iter->next = uj;
}
void removeFromPath(Path *path, Path *remove)
{
    Path *iter, *lemarado;
    iter = path->next;
    lemarado = path;

    while(iter != NULL)
    {
        Path *next = iter->next;
        if(iter == remove)
        {
            lemarado->next = next;
            free(iter);
        }
        iter = next;
    }
}
int PathIsValid(int row, int col, int maxx, int maxy)
{
    return (row >= 0 && col >= 0 && row< 50 && col < 50);
}
void freePaths(Path *path)
{
    Path *iter;
    iter = path->next;
    while(iter != NULL)
    {
        Path *next = iter->next;
        free(iter);
        iter = next;
    }
    free(path);
}
Position getEnemyDirection(World *world, Character *Enemy)
{
    int minx, miny, maxx, maxy, i ,j;
    Path *path;
    Position direction = {0, 0};
    int found = 0;

    //Str�zsa
    path = (Path*) malloc(sizeof(Path));
    path->next = NULL;


    /*maxx=(world->min.x+SCREEN_WIDTH)/BLOCK_SIZE;
    maxy=(world->min.y+SCREEN_HEIGHT)/BLOCK_SIZE;*/

    int map[50][50];



    j = 0;
    for(miny = 0; miny < world->max.y/BLOCK_SIZE; miny++)
    {
        i = 0;

        for(minx = 0; minx < world->max.x/BLOCK_SIZE; minx++)
        {
            //boxColor(screen, i*BLOCK_SIZE, j*BLOCK_SIZE, i*BLOCK_SIZE+64, j*BLOCK_SIZE+64, 0x0000ffff);

            if(world->object[miny][minx].type == SOLID)
            {
                map[j][i] = 2;
                /*map[j-1][i] = 1;
                map[j][i-1] = 1;*/
                i++;
            }
            else
               map[j][i++] = 0;

        }
        j++;

    }


    Path *iter;
   /* minx = (world->player.position.x-world->min.x+32)/BLOCK_SIZE;
    miny = (world->player.position.y-world->min.y+32)/BLOCK_SIZE;*/
    minx = (world->player.position.x+32)/BLOCK_SIZE;
    miny = (world->player.position.y+32)/BLOCK_SIZE;

    AddToPath(path, miny, minx);
    map[miny][minx] = 1;
    iter = path->next;
    //map[(Enemy->position.y-world->min.y+32)/BLOCK_SIZE][(Enemy->position.x-world->min.x+32)/BLOCK_SIZE] = 3;
    while(iter != NULL && found == 0)
    {
        if(iter->col == (Enemy->position.x)/BLOCK_SIZE && iter->row-1 == (Enemy->collisionRect.y)/BLOCK_SIZE)
        {
            if(world->object[iter->row][iter->col+1].type == SOLID &&isCollidingWith(Enemy->collisionRect, world->object[iter->row][iter->col+1].collisionRect))
                direction.x = -1;
            else
                direction.y = 1;
            found = 1;
            break;
        }
        else if (iter->col+1 == (Enemy->collisionRect.x)/BLOCK_SIZE && iter->row == (Enemy->collisionRect.y)/BLOCK_SIZE)
        {
            if(world->object[iter->row+1][iter->col].type == SOLID && isCollidingWith(Enemy->collisionRect, world->object[iter->row+1][iter->col].collisionRect))
                direction.y = -1;
            else
                direction.x = -1;
            found = 1;
            break;
        }
        else if (iter->col == (Enemy->collisionRect.x)/BLOCK_SIZE && iter->row+1 == (Enemy->collisionRect.y)/BLOCK_SIZE)
        {
            if(world->object[iter->row][iter->col+1].type == SOLID && isCollidingWith(Enemy->collisionRect, world->object[iter->row][iter->col+1].collisionRect))
                direction.x = -1;
            else
                direction.y = -1;
            found = 1;
            break;
        }
        else if (iter->col-1 == (Enemy->collisionRect.x)/BLOCK_SIZE && iter->row == (Enemy->collisionRect.y)/BLOCK_SIZE)
        {
            if(world->object[iter->row+1][iter->col].type == SOLID && isCollidingWith(Enemy->collisionRect, world->object[iter->row+1][iter->col].collisionRect))
                direction.y = -1;
            else
                direction.x = 1;
            found = 1;
            break;
        }
        if(PathIsValid(iter->row-1,iter->col, maxx, maxy) && map[iter->row-1][iter->col] == 0)
        {
            AddToPath(path, iter->row-1, iter->col);
            map[iter->row-1][iter->col] = 1;
        }
        if(PathIsValid(iter->row,iter->col+1, maxx, maxy) &&map[iter->row][iter->col+1] == 0)
        {
            AddToPath(path, iter->row, iter->col+1);
            map[iter->row][iter->col+1] = 1;
        }
        if(PathIsValid(iter->row+1,iter->col, maxx, maxy) && map[iter->row+1][iter->col] == 0)
        {
            AddToPath(path, iter->row+1, iter->col);
            map[iter->row+1][iter->col] = 1;
        }
        if(PathIsValid(iter->row,iter->col-1, maxx, maxy) && map[iter->row][iter->col-1] == 0)
        {
            AddToPath(path, iter->row, iter->col-1);
            map[iter->row][iter->col-1] = 1;
        }


        //boxColor(screen, (iter->col+world->min.x/BLOCK_SIZE)*BLOCK_SIZE-world->min.x, (iter->row+world->min.y/BLOCK_SIZE)*BLOCK_SIZE-world->min.y,
                    //(iter->col+world->min.x/BLOCK_SIZE)*BLOCK_SIZE-world->min.x+64, (iter->row+world->min.y/BLOCK_SIZE)*BLOCK_SIZE-world->min.y+64, 0x00ffffff);

        Path *next = iter->next;
        removeFromPath(path, iter);
        iter = next;
    }
   /* j = 0;
    for(miny = 0; miny < world->max.y/BLOCK_SIZE; miny++)
    {
        i = 0;

        for(minx = 0; minx < world->max.x/BLOCK_SIZE; minx++)
        {
            char string[5];
            sprintf(string, "%d", map[j][i]);
            stringRGBA(screen, (i)*BLOCK_SIZE-world->min.x, (j)*BLOCK_SIZE-world->min.y, string, 255, 55, 255, 255);
            i++;
        }
        j++;
    }*/
    char string2[50];
        sprintf(string2, "%d %d", direction.x, direction.y);
            stringRGBA(screen, 150, 10, string2, 255, 255, 255, 255);

    //boxColor(screen, world->min.x+iter->col*BLOCK_SIZE, world->min.y+iter->row*BLOCK_SIZE, world->min.x+iter->col*BLOCK_SIZE+64, world->min.y+iter->row*BLOCK_SIZE+64, 0x0000ffff);
    //boxColor(screen, (minx+world->min.x/BLOCK_SIZE)*BLOCK_SIZE-world->min.x, (miny+world->min.y/BLOCK_SIZE)*BLOCK_SIZE-world->min.y, (minx+world->min.x/BLOCK_SIZE)*BLOCK_SIZE+64-world->min.x, (miny+world->min.y/BLOCK_SIZE)*BLOCK_SIZE+64-world->min.y, 0xff00ffff);

    /*if(iter == NULL)
    {
        Position dest = {BLOCK_SIZE, BLOCK_SIZE};
        return dest;
    }
    else
    {*/
    //TODO Ez sajnos nem t�k�letes :(
    //if(Enemy->collisionRect.x/BLOCK_SIZE == (world->player.position.x+32)/BLOCK_SIZE && Enemy->collisionRect.y/BLOCK_SIZE == (world->player.position.y+32)/BLOCK_SIZE)
    if(isNear(Enemy->position, world->player.position, 64))
    {
         if(world->player.position.x >= Enemy->position.x)
        {
            if(world->player.position.y > Enemy->position.y)
            {
                direction.x = 0;
                direction.y = 1;
            }
            else
            {
                direction.x = 1;
                direction.y = 0;
            }

        }
        else
        {
            if(world->player.position.y < Enemy->position.y)
            {
                direction.x = 0;
                direction.y = -1;
            }
            else //if (player->position.x < enemy->position.x)
            {
                direction.x = -1;
                direction.y = 0;
            }
        }
    }

    freePaths(path);
    return direction;
    //}


}
int isNear(Position p1, Position p2, int distance)
{
    return abs((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y)) <= distance*distance;
}

void moveEnemy(World *world, Character *enemy, Character *player)
{

    Rect collisionRect = {enemy->position.x+16, enemy->position.y+40, 32, 24};
    //Nagyobb player collitionrectet �ll�tunk be
    //Rect playerCollisionRect = {player->position.x+16, player->position.y+10, 32, 54};
    enemy->collisionRect = collisionRect;
    Position nextpos = enemy->position;
    Position nextdest;
    Position direction = {0, 0};

    nextdest.x = enemy->position.x;
    nextdest.y = enemy->position.y;


    //TODO: a character classnak egy direction adattag, amit k�vet
        //random directionokat gener�lni egy kis k�rzetben, hogy mozogjon,
        //am�g a player nincs el�g k�zel
    //if(abs(enemy->destination.x -500) <= enemy->position.x && abs(enemy->destination.y -500) <= enemy->position.y)

    if((isNear(player->position, enemy->position, 512) || enemy->isFollowing) && !player->isDead)
    {
        direction = getEnemyDirection(world, enemy);
        enemy->isFollowing = 1;
    }
        //ha l�t minket az ellens�g (a k�pernyon van), akkor fl�nk veszi az ir�nyt
        //enemy->destination = player->position;
//        if(player->position.y >= enemy->position.y)
//        {
//            nextdest.y +=64;
//            /*if(nextdest.y > player->position.y)
//            {
//                if(player->position.x >enemy->position.x)
//                    nextdest.x += 64;
//                else
//                    nextdest.x -= 64;
//            }*/
//        }
//        else if(player->position.y < enemy->position.y)
//        {
//            nextdest.y -=64;
//            if(nextdest.y < player->position.y)
//            {
//                if(player->position.x >enemy->position.x)
//                    nextdest.x += 64;
//                else
//                    nextdest.x -= 64;
//            }
//        }
       /* else if(player->position.x >enemy->position.x)
            nextdest.x += 64;
        else if (player->position.x <= enemy->position.x)
            nextdest.x -= 64;*/

        /*if(abs(player->position.y-enemy->position.y)<10)
        {
            if(player->position.x>= enemy->position.x)
            {
                nextdest.x++;
            }
            else
                nextdest.x--;
        }
        else
        {
             if(player->position.y>= enemy->position.y)
            {
                nextdest.y++;
            }
            else
                nextdest.y--;

        }*/


        /* if(player->position.x >= enemy->position.x)
        {
            if(player->position.y > enemy->position.y)
            {
                nextdest.y++;
            }
            else
            {
                nextdest.x++;
            }

        }
        else
        {
            if(player->position.y < enemy->position.y)
            {
                nextdest.y--;
            }
            else //if (player->position.x < enemy->position.x)
            {
                nextdest.x--;
            }
        }*/



        /*collisionRect.x = nextdest.x+16;
        collisionRect.y = nextdest.y+40;
        collisionRect.w = 32;
        collisionRect.h = 24;
        if(isColliding(world, collisionRect))
        {
            if(enemy->destination.x == nextdest.x)
            {
                nextdest.y = enemy->position.y;
                nextdest.x +=1;
            }
            else if(enemy->destination.y == nextdest.y)
            {
                nextdest.x = enemy->position.x;
                nextdest.y +=1;
            }

        }*/
    //}
    //enemy->destination = nextdest;

    if(isCollidingWith(enemy->collisionRect, player->collisionRect) && !player->isDead)
    {
        //a player "lot�volban" van �s m�g �l, t�madjuk meg!

        if(enemy->moveid == UP) {
            enemy->moveid = THRUST_UP;
            enemy->hitBox.x = enemy->position.x + 36;
            enemy->hitBox.y = enemy->position.y;
            enemy->hitBox.w = 7;
            enemy->hitBox.h = 55;
        }
        else if(enemy->moveid == LEFT) {
            enemy->moveid = THRUST_LEFT;
            enemy->hitBox.x = enemy->position.x;
            enemy->hitBox.y = enemy->position.y+41;
            enemy->hitBox.w = 45;
            enemy->hitBox.h = 7;
        }
        else if(enemy->moveid == DOWN) {
            enemy->moveid = THRUST_DOWN;
            enemy->hitBox.x = enemy->position.x+21;
            enemy->hitBox.y = enemy->position.y+32;
            enemy->hitBox.w = 7;
            enemy->hitBox.h = 32;
        }
        else if(enemy->moveid == RIGHT) {
            enemy->moveid = THRUST_RIGHT;
            enemy->hitBox.x = enemy->position.x+32;
            enemy->hitBox.y = enemy->position.y+41;
            enemy->hitBox.w = 32;
            enemy->hitBox.h = 7;
        }

        collisionRect.x = player->position.x+16;
        collisionRect.y = player->position.y+20;
        collisionRect.w = 32;
        collisionRect.h = 44;
        //TODO: Lehetne szebben is csin�lni
        int endofanimation = updateFrames(enemy, 1);
        if(isCollidingWith(collisionRect, enemy->hitBox) && endofanimation )
            //eltal�ltuk
            player->health -= 5;


    }
    else
    {
        /*if(enemy->destination.x +4 >= enemy->position.x &&
            enemy->destination.y +4 >= enemy->position.y &&
            enemy->position.x +4 >= enemy->destination.x &&
            enemy->position.y +4 >= enemy->destination.y)

        {
            //az ellens�g el�rte a c�lj�t, adjunk �jjat neki
            setEnemyDestination(world, enemy);
        }*/

//        if(enemy->destination.x >= enemy->position.x)
//        {
//            if(enemy->destination.y > enemy->position.y)
//            {
//                enemy->moveid = DOWN;
//                //enemy->position.y += 4;
//                nextpos.y += 4;
//            }
//            else
//            {
//                enemy->moveid = RIGHT;
//                //enemy->position.x += 4;
//                nextpos.x += 4;
//            }
//
//        }
//        else
//        {
//            if(enemy->destination.y < enemy->position.y)
//            {
//                enemy->moveid = UP;
//                //enemy->position.y -= 4;
//                nextpos.y -= 4;
//            }
//            else //if (player->position.x < enemy->position.x)
//            {
//                enemy->moveid = LEFT;
//                //enemy->position.x -= 4;
//                nextpos.x -= 4;
//            }
//        }

        if(direction.x == 1)
        {
            enemy->moveid = RIGHT;
        }
        else if(direction.x == -1)
        {
            enemy->moveid = LEFT;
        }
        else if(direction.y == -1)
        {
            enemy->moveid = UP;
        }
        else if (direction.y == 1)
        {
            enemy->moveid = DOWN;
        }



        nextpos.x += enemy->speed*direction.x;
        nextpos.y += enemy->speed*direction.y;


       /* if(isColliding(world, collisionRect))
        {
            //Az ellens�g akad�lynak �tk�z�tt, ki kell ker�lni azt
            if(nextpos.x == enemy->position.x)
            {
                nextpos.x += 4;
                enemy->destination.x += 64;
                enemy->destination.y = enemy->position.y;
                nextpos.y = enemy->position.y;

            }
            else if (nextpos.y == enemy->position.y)
            {
                nextpos.y += 4;
                nextpos.x = enemy->position.x;
                enemy->destination.y += 64;
                enemy->destination.x = enemy->position.x;
            }



        }
        else enemy->destination = player->position;*/
        if(direction.x || direction.y)
            updateFrames(enemy, 1);
        else
            enemy->currentFrame = 0;
    }
    enemy->position = nextpos;





}
void drawEnemyHealthbar(World *world, SDL_Surface *screen, Character character)
{
    int x1, y1, x2, y2, margin, w, h, offset;
    margin = 1;
    offset = 3;
    h = 3+2*margin;
    x1 = character.position.x-offset;
    y1 = character.position.y-h;
    w = BLOCK_SIZE+2*margin+2*offset;
    x2 = x1+w;
    y2 = y1+h;
    rectangleColor(screen, x1-world->min.x, y1-world->min.y, x2-world->min.x, y2-world->min.y, 0xffffffff);
    boxColor(screen, x1+margin-world->min.x, y1+margin-world->min.y,
             x1+(double)character.health/character.maxHealth*(w-margin)-world->min.x, y2-margin-world->min.y, 0x00ff00ff);
}
void characterDraw(World *world, SDL_Surface *screen, Character character)
{

    SDL_Rect src = { (character.movetype[character.moveid].startX + character.currentFrame)*64, character.movetype[character.moveid].startY*64, 64, 64 };
    SDL_Rect dest = { character.position.x-world->min.x, character.position.y-world->min.y, 64, 64 };

    SDL_BlitSurface(character.IMG, &src, screen, &dest);
    #ifdef _SHOWCOLLISIONRECTS
    char text[100];
    sprintf(text, "%d", character.moveid);
    stringRGBA(screen, 100, 100, text, 255, 255, 255, 255);
    //character's collision box

    rectangleColor(screen, character.collisionRect.x-world->min.x, character.collisionRect.y-world->min.y, character.collisionRect.x-world->min.x+character.collisionRect.w, character.collisionRect.y-world->min.y+character.collisionRect.h, 0xff0000ff);
    //character's hitbox
    rectangleColor(screen, character.hitBox.x-world->min.x, character.hitBox.y-world->min.y, character.hitBox.x-world->min.x+character.hitBox.w, character.hitBox.y-world->min.y+character.hitBox.h, 0xff0000ff);
    #endif // _SHOWCOLLISIONRECTS
}


int updateFrames(Character *character, int repeat) //1-el t�r vissza, ha az animacio veget ert
{
    if(character->currentFrame >= character->movetype[character->moveid].numberOfFrames-1)
    {
        if(repeat) character->currentFrame = 0;
        return 1;
    }
    else
    {
        character->currentFrame +=1;
        return 0;
    }
}


void updateCharacters(World *world)
{
    int i;
    EnemyListaElem *mozgo;
    ItemListaElem *currentItem;


    world->player.collisionRect.x = world->player.position.x+16;
    world->player.collisionRect.y = world->player.position.y+40;
    world->player.collisionRect.w = 32;
    world->player.collisionRect.h = 24;

    //for(mozgo = world->enemies->elso->kov; mozgo != world->enemies->utolso; mozgo = mozgo->kov)
    mozgo = world->enemies.elso->kov;
    while(mozgo != world->enemies.utolso)
    {
            if(!mozgo->Enemy.isDead)
            {
                moveEnemy(world, &mozgo->Enemy, &world->player);

            }

            //megn�zz�k, hogy nem-tallt�k-e el
            //TODO: el�g lenne csak a k�pernyon l�vo ellens�gek, �s nyilak vizsg�lata
            ArrowListaElem *arrow;

            EnemyListaElem *enemytemp = mozgo->kov;

            arrow = world->arrows.elso->kov;
            while(arrow !=world->arrows.utolso)
            {
                ArrowListaElem *arrowtemp = arrow->kov;
                //Egy nagyobb collisionrectel kell az nyilaknak
                Rect enemyCollisionRect = {mozgo->Enemy.collisionRect.x, mozgo->Enemy.collisionRect.y-20,
                    mozgo->Enemy.collisionRect.w, mozgo->Enemy.collisionRect.h+20};
                if(!isOutOfScreen(world, mozgo->Enemy.position) && isCollidingWith(enemyCollisionRect, arrow->arrows.collisionRect) && !mozgo->Enemy.isDead)
                {
                    //az ellens�g meghal, az arrow eltunik
                     removeArrow(arrow);
                    mozgo->Enemy.health -= 10;
                    if(mozgo->Enemy.health <= 0)
                    {
                        mozgo->Enemy.isDead = 1;
                        mozgo->Enemy.moveid = DIE;
                        mozgo->Enemy.currentFrame = 0;
                        if(mozgo->Enemy.itemDrop == DIAMOND)
                        {
                            AddItem(world, mozgo->Enemy.position, 8, DIAMOND);
                        }
                    }
                }
                arrow = arrowtemp;
            }
            if(mozgo->Enemy.isDead)
            {
                if(updateFrames(&mozgo->Enemy, 0) && isOutOfScreen(world, mozgo->Enemy.position))
                //v�ge az anim�ci�nak, �s m�r nem l�tjuk. Elt�vol�tjuk a halottat
                    removeEnemy(mozgo);

            }

            mozgo = enemytemp;

    }



   for(currentItem = world->items.elso->kov; currentItem!=world->items.utolso;
                    currentItem=currentItem->kov)
        updateItemFrames(&currentItem->item, 1);



    if(!world->player.isDead)
        move(world);

    moveArrows(world);

    if(world->player.health <=0 && !world->player.isDead)
    {
        world->player.isDead = 1;
        world->player.moveid = DIE;
        world->player.currentFrame = 0;

    }
    if(world->player.isDead && updateFrames(&world->player, 0))
            //Az anim�ci� v�get �rt
            GameState = END;


}

void setEnemyDestination(World *world, Character *enemy)
{
    //Gener�lunk egy random poz�ci�t az ellens�g150 sugar� k�rnyezet�ben, ahova az ellens�g megy
    Position dest;

    dest.x = rand()%300-150;
    dest.y = rand()%300-150;
    enemy->destination.x = enemy->position.x +dest.x;
    enemy->destination.y = enemy->position.y +dest.y;
    while(isOutOfMap(world, enemy->destination))
    {
        dest.x = rand()%300-150;
        dest.y = rand()%300-150;
        enemy->destination.x = enemy->position.x +dest.x;
        enemy->destination.y = enemy->position.y +dest.y;
    }

}
void generateEnemies(World *world)
{
    int x, y;
    int i;
    EnemyType type;
    int maxHealth;

    for(i=0; i < world->numberOfEnemies; i++)
    {
        x = rand()%(world->max.x/BLOCK_SIZE );
        y = rand()%(world->max.y/BLOCK_SIZE );

        while(world->object[y][x].type != WALKABLE)
        {
            //am�g van valami az adott helye, addig m�s helyet kell gener�lni
            x = rand()%(world->max.x/BLOCK_SIZE );
            y = rand()%(world->max.y/BLOCK_SIZE );
        }
        Position position;
        position.x = x*BLOCK_SIZE;
        position.y = y*BLOCK_SIZE;

        type = rand()%3;
        if(type == GOLD)
        {
            maxHealth = 40;
        }
        else
        {
            maxHealth = 20;
        }
        AddEnemy(world, position, type, DOWN, 0, maxHealth, maxHealth, 0, position, 0);
    }
}
