#include "world.h"

int isCollidingWith(Rect r1, Rect r2)
{
    return ((r1.x+r1.w >= r2.x && r1.y+r1.h >= r2.y) &&
            (r2.x+r2.w >= r1.x && r2.y+r2.h >= r1.y));
}
int isColliding(World *world, Rect rect)
{
    int i, j;
    Position min, max;

    min.x = world->min.x;
    max.x = world->min.x + SCREEN_WIDTH +(world->min.x +SCREEN_WIDTH>=world->max.x ? 0 : 64);

    min.y = world->min.y;
    max.y = world->min.y + SCREEN_HEIGHT +(world->min.y +SCREEN_HEIGHT>=world->max.y ? 0 : BLOCK_SIZE);

    for(j = min.y / BLOCK_SIZE; j < max.y/BLOCK_SIZE; j++)
    {
        for(i = min.x / BLOCK_SIZE; i < max.x/BLOCK_SIZE; i++)
        {
            //A k�pernyon levo �sszes SOLID t�gyra megn�zz�k az �tk�z�st
            if(world->object[j][i].type == SOLID)
                if(isCollidingWith(world->object[j][i].collisionRect, rect))
                    return 1;

        }
      }
      return 0;
}


int isOutOfMap(World *world, Position position)
{
    return (position.y >= world->max.y-64 || position.y <= 0
            || position.x <= 0 || position.x >= world->max.x-64);
}
int isOutOfScreen(World *world, Position position)
{
    return (position.x < world->min.x-64 || position.x > world->min.x+SCREEN_WIDTH ||
                position.y < world->min.y-64 || position.y > world->min.y +SCREEN_HEIGHT);
}

void drawScreen(World *world, SDL_Surface *screen, Layer layer)
{
    int i, j;
    Position min, max;


    //TODO: 640*480 as kijelzon is muk�dj�n. Most csak 64 t�bsz�r�se nagys�gu kijelzon uk�dik


    min.x = world->min.x;
    max.x = world->min.x + SCREEN_WIDTH +(world->min.x +SCREEN_WIDTH>=world->max.x ? 0 : BLOCK_SIZE);

    min.y = world->min.y;
    max.y = world->min.y + SCREEN_HEIGHT +(world->min.y +SCREEN_HEIGHT>=world->max.y ? 0 : BLOCK_SIZE);
    for(j = min.y / BLOCK_SIZE; j < max.y/BLOCK_SIZE; j++)
    {
        for(i = min.x / BLOCK_SIZE; i < max.x/BLOCK_SIZE; i++)
        {
            if(world->objectBottom[j][i].id && world->objectBottom[j][i].layer == layer)
            {
                    //Haa van valami alatta, ki kell rajzolni elobb azt
                    drawobject(world->objectBottom[j][i], world->min, world->texture, screen);

            }
            if (world->object[j][i].layer == layer)// !=0 && world->map[j][i] !=-1)
            {

                drawobject(world->object[j][i], world->min, world->texture, screen);
            }
        }
    }
}

void draw(World *world, SDL_Surface *screen)
{
    int i;
    int db = 0;
    EnemyListaElem *mozgo;
    ItemListaElem *currentItem;
    drawScreen(world, screen, BACK);

    drawArrows(world, screen);

    for(mozgo = world->enemies.elso->kov; mozgo != world->enemies.utolso; mozgo = mozgo->kov)
    {
        db++;
        //if(!isOutOfScreen(world, mozgo->Enemy.position))
            characterDraw(world, screen, mozgo->Enemy);
            if(!mozgo->Enemy.isDead && mozgo->Enemy.health < mozgo->Enemy.maxHealth)
                drawEnemyHealthbar(world, screen, mozgo->Enemy);
        #ifdef _SHOWCOLLISIONRECTS
        char text2[100];
        sprintf(text2, "destx: %d, desty: %d",  mozgo->Enemy.destination.x, mozgo->Enemy.destination.y);
        stringRGBA(screen, 10, 480, text2, 255, 255, 255, 255);
        #endif
    }
    #ifdef _SHOWCOLLISIONRECTS
    char text[100];
    sprintf(text, "x: %d, y: %d currentframe: %d enemies: %d",  world->player.collisionRect.x, world->player.collisionRect.y, world->player.currentFrame, db);
    stringRGBA(screen, 10, 500, text, 255, 255, 255, 255);
    #endif



    characterDraw(world, screen, world->player);
    db = 0;
   for(currentItem = world->items.elso->kov; currentItem!=world->items.utolso;
                    currentItem=currentItem->kov)
    {
        db++;
        if(!isOutOfScreen(world, currentItem->item.position))
            drawItems(world, screen, currentItem->item);

    }
    #ifdef _SHOWCOLLISIONRECTS
    sprintf(text, "Items: %d", db);
    stringRGBA(screen, 430, 500, text, 255, 255, 255, 255);
    #endif

    drawScreen(world, screen, FRONT);
    drawMenus(screen);

}

void inizializeLists(World *world)
{
    //Be�ll�tjuk az ArrowList-ben a str�zs�t
    world->arrows.elso = (ArrowListaElem*) malloc(sizeof(ArrowListaElem));
    world->arrows.utolso = (ArrowListaElem*) malloc(sizeof(ArrowListaElem));
    world->arrows.elso->kov = world->arrows.utolso;
    world->arrows.utolso->elozo = world->arrows.elso;

    //Be�ll�tjuk az EnemyList-ben a str�zs�t
    world->enemies.elso = (EnemyListaElem*) malloc(sizeof(EnemyListaElem));
    world->enemies.utolso = (EnemyListaElem*) malloc(sizeof(EnemyListaElem));
    world->enemies.elso->kov = world->enemies.utolso;
    world->enemies.utolso->elozo = world->enemies.elso;

    //Be�ll�tjuk az ItemList-ben a str�zs�t
    world->items.elso = (ItemListaElem*) malloc(sizeof(ItemListaElem));
    world->items.utolso = (ItemListaElem*) malloc(sizeof(ItemListaElem));
    world->items.elso->kov = world->items.utolso;
    world->items.utolso->elozo = world->items.elso;
}
int initializeWorld(World *world)
{

    inizializeLists(world);

    world->numberOfEnemies = 25;
    world->maxDiamonds = 50;
    world->numberOfDiamonds = 0;

    world->min.x = 0;
    world->min.y = 0;

    Position position = {250, 250};
    AddPlayer(world, position, DOWN, 0, 100, 0, 0, 5, 100); //Beadjuk a j�t�kost az alapbe�ll�t�sokkal
    loadWorld(world);

    generateEnemies(world);

    generateDiamonds(world);

    Position pos = {512, 512};
    //AddEnemy(world, pos, EnemyIMG, DOWN, 0, 20, 20, 0, pos, 4, 0);
    //AddItem(world, pos, 8, DIAMOND);


    return 1;
}
int SaveGame(World *world)
{
    FILE *fp;

    fp = fopen("save.dat", "wb");
    if (fp == NULL) {
       perror("save.dat megnyit�sa");
       return 0;
    }

    fwrite(&world->min, sizeof(Position), 1, fp);
    fwrite(&world->maxDiamonds, sizeof(int), 1, fp);

    //Player elment�se
    fwrite(&world->player.position, sizeof(Position), 1, fp);
    fwrite(&world->player.moveid, sizeof(moveId), 1, fp);
    fwrite(&world->player.speed, sizeof(double), 1, fp);
    fwrite(&world->player.currentFrame, sizeof(int), 1, fp);
    fwrite(&world->player.diamonds, sizeof(int), 1, fp);
    fwrite(&world->player.health, sizeof(int), 1, fp);
    fwrite(&world->player.isDead, sizeof(int), 1, fp);
    fwrite(&world->player.stamina, sizeof(int), 1, fp);

    EnemyListaElem *mozgo;
    int db = 0;;
    for(mozgo = world->enemies.elso->kov; mozgo != world->enemies.utolso; mozgo = mozgo->kov)
    {
        db++;
    }

    fwrite(&db, sizeof(int), 1, fp);

    for(mozgo = world->enemies.elso->kov; mozgo != world->enemies.utolso; mozgo = mozgo->kov)
    {
        fwrite(&mozgo->Enemy.position, sizeof(Position), 1, fp);
        fwrite(&mozgo->Enemy.moveid, sizeof(moveId), 1, fp);
        fwrite(&mozgo->Enemy.type, sizeof(EnemyType), 1, fp);
        fwrite(&mozgo->Enemy.currentFrame, sizeof(int), 1, fp);
        fwrite(&mozgo->Enemy.hitBox, sizeof(Rect), 1, fp);
        fwrite(&mozgo->Enemy.health, sizeof(int), 1, fp);
        fwrite(&mozgo->Enemy.maxHealth, sizeof(int), 1, fp);
        fwrite(&mozgo->Enemy.isDead, sizeof(int), 1, fp);
        fwrite(&mozgo->Enemy.isFollowing, sizeof(int), 1, fp);
        fwrite(&mozgo->Enemy.destination, sizeof(Position), 1, fp);
    }

    ItemListaElem *iter;
    db = 0;;
    for(iter = world->items.elso->kov; iter != world->items.utolso; iter = iter->kov)
    {
        db++;
    }

    fwrite(&db, sizeof(int), 1, fp);

    for(iter = world->items.elso->kov; iter != world->items.utolso; iter = iter->kov)
    {
        fwrite(&iter->item.position, sizeof(Position), 1, fp);
        fwrite(&iter->item.id, sizeof(ItemID), 1, fp);
        fwrite(&iter->item.numberOfFrames, sizeof(int), 1, fp);

    }

    fclose(fp);
    return 1;
}
int LoadGame(World *world)
{
    FILE *fp;

    fp = fopen("save.dat", "rb");


    fread(&world->min, sizeof(Position), 1, fp);
    fread(&world->maxDiamonds, sizeof(int), 1, fp);
    if (fp == NULL) {
       perror("save.dat megnyit�sa");
       return 0;
    }


    //Player bet�lt�se
    Character player;
    fread(&player.position, sizeof(Position), 1, fp);
    fread(&player.moveid, sizeof(moveId), 1, fp);
    fread(&player.speed, sizeof(double), 1, fp);
    fread(&player.currentFrame, sizeof(int), 1, fp);
    fread(&player.diamonds, sizeof(int), 1, fp);
    fread(&player.health, sizeof(int), 1, fp);
    fread(&player.isDead, sizeof(int), 1, fp);
    fread(&player.stamina, sizeof(int), 1, fp);

    AddPlayer(world, player.position, player.moveid, player.currentFrame, player.health, player.isDead, player.diamonds, player.speed, player.stamina);

    //Ellens�gek beolvas�sa
    Character Enemy;
    int i;
    int db;
    fread(&db, sizeof(int), 1, fp);
    for(i = 0; i < db; i++)
    {
        fread(&Enemy.position, sizeof(Position), 1, fp);
        fread(&Enemy.moveid, sizeof(moveId), 1, fp);
        fread(&Enemy.type, sizeof(EnemyType), 1, fp);
        fread(&Enemy.currentFrame, sizeof(int), 1, fp);
        fread(&Enemy.hitBox, sizeof(Rect), 1, fp);
        fread(&Enemy.health, sizeof(int), 1, fp);
        fread(&Enemy.maxHealth, sizeof(int), 1, fp);
        fread(&Enemy.isDead, sizeof(int), 1, fp);
        fread(&Enemy.isFollowing, sizeof(int), 1, fp);
        fread(&Enemy.destination, sizeof(Position), 1, fp);
        AddEnemy(world, Enemy.position, Enemy.type, Enemy.moveid, Enemy.currentFrame, Enemy.health, Enemy.maxHealth, Enemy.isDead, Enemy.destination, Enemy.isFollowing);
    }
    //Itemek bet�lt�se
    Item item;

    fread(&db, sizeof(int), 1, fp);
    for(i = 0; i < db; i++)
    {
        fread(&item.position, sizeof(Position), 1, fp);
        fread(&item.id, sizeof(ItemID), 1, fp);
        fread(&item.numberOfFrames, sizeof(int), 1, fp);

        AddItem(world, item.position, item.numberOfFrames, item.id);
    }

    fclose(fp);
    return 1;
}
void loadWorld(World *world)
{
    FILE *fp;
    char *palya;
    int i, j, hossz, palyax, palyay;

    char *slash;
    int type, id;
    char *block;


    fp = fopen("map.txt", "rt");
    if (fp == NULL) {
        perror("map.txt megnyit�sa");
        return;
    }

    fscanf(fp, "%dx%d", &palyax, &palyay);

    hossz = 0;
    while(fgetc(fp) != EOF)
        hossz++;

    fclose(fp);

    fp = fopen("map.txt", "rt");

    palya = (char*) malloc(sizeof(char)*hossz+1);
    i = 0;
    while(fgetc(fp) != '\n'); //Elugrunk az els sor v�g�re, mert itt csak a m�retek vannak.

    for(i = 0; i < hossz; i++)
        fscanf(fp, "%c", &palya[i]);

    palya[i] = '\0';
    fclose(fp);

    //Lefoglaljuk a 2D-s t�mb�t
    world->object = (Object**) malloc(sizeof(Object*)*palyay);
    for(i = 0; i <palyay; i++)
        world->object[i] = (Object*) malloc(sizeof(Object)*palyax);

    world->objectBottom = (Object**) malloc(sizeof(Object*)*palyay);
    for(i = 0; i <palyay; i++)
        world->objectBottom[i] = (Object*) malloc(sizeof(Object)*palyax);

    world->max.x = palyax*64;
    world->max.y = palyay*64;

    i=j= 0;

    block = strtok (palya," ");
    while (block != NULL)
    {
        //elv�lasztjuk a / jelek ment�n
        //id/type/bottomid form�tumban

            id =atoi(block);
            slash = strchr(block, '/');
            type = (slash ? atoi(slash+1) : 0);
             if(slash)
            {
                //a felhaszn�l� megadott m�g egy objectet
                slash = strchr(slash+1, '/');
                createObject(&world->objectBottom[j][i], (slash ? atoi(slash+1) : 0), i*64, j*64, 2);
            }
        createObject(&world->object[j][i], id, i*64, j*64, type);

        i++;
        if(i == palyax)
        {
            j++;
            i = 0;
        }
        block = strtok (NULL, " ");
    }
}
