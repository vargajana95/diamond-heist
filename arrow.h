#ifndef ARROW_H_INCLUDED
#define ARROW_H_INCLUDED

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_gfxPrimitives.h>
#include "main.h"

//L�trehozok  dup�n l�ncolt list�kat.
//Ezt haszn�lva fogom sz�montartani az ellens�geket, illetve a kilott ny�lvesszoket.
typedef struct Arrow{

    Position position;
    Rect collisionRect;
    Direction direction;

} Arrow;
typedef struct ArrowListaElem {
    Arrow arrows;
    struct ArrowListaElem *elozo, *kov;
} ArrowListaElem;
typedef struct ArrowList {
    ArrowListaElem *elso;
    ArrowListaElem *utolso;
} ArrowList;

typedef struct World World;

void addArrow(World *world, Position position, Direction direction);
void removeArrow(ArrowListaElem *arrow);
void drawArrows(World *world, SDL_Surface *screen);
void moveArrows(World *world);
void freeArrowList(ArrowList *lista);


#endif // ARROW_H_INCLUDED
