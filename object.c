#include "object.h"
#include "world.h"

void createObject(Object *object, objectId id, int x, int y, Type type)
{
    object->id = id;
    object->type = type;
    object->sheetPos.x = 64*(id-1)%SHEET_SIZE;
    object->sheetPos.y = 64*(id-1)/SHEET_SIZE*64;
    object->sheetPos.w = 64;
    object->sheetPos.h = 64;
    object->position.x = x;
    object->position.y = y;
    object->collisionRect.x = object->position.x;
    object->collisionRect.y = object->position.y;
    object->collisionRect.w = object->sheetPos.w;
    object->collisionRect.h = object->sheetPos.h;
    object->layer = ((type == SOLID || type == WALKABLE) ? BACK : FRONT);
    //speci�lis esetek, k�l�n kell be�ll�tani a cllision rect m�ret�ket
    if(id == GRASS || id == 53)
    {
        object->sheetPos.x = 0;
        object->sheetPos.y = 0;
        object->layer = BACK;
        object->type = WALKABLE;
    }
    else if (id == TREE)
    {
        object->sheetPos.w = 128;
        object->sheetPos.h = 128;
    }
    else if (id == ROCK1)
    {
        object->collisionRect.x = object->position.x+20;
        object->collisionRect.y = object->position.y+20;
        object->collisionRect.w = 20;
        object->collisionRect.h = 35;
    }
    else if (id == WATER_TOP_LEFT)
    {
        object->collisionRect.x = object->position.x+25;
        object->collisionRect.y = object->position.y+25;
        object->collisionRect.w = 39;
        object->collisionRect.h = 39;
    }
    else if (id == WATER_TOP_RIGHT)
    {
        object->collisionRect.x = object->position.x;
        object->collisionRect.y = object->position.y+25;
        object->collisionRect.w = 39;
        object->collisionRect.h = 39;
    }
    else if (id == WATER_TOP)
    {
        object->collisionRect.x = object->position.x;
        object->collisionRect.y = object->position.y+25;
        object->collisionRect.w = 64;
        object->collisionRect.h = 39;
    }
    else if (id == WATER_LEFT)
    {
        object->collisionRect.x = object->position.x+25;
        object->collisionRect.y = object->position.y;
        object->collisionRect.w = 39;
        object->collisionRect.h = 64;
    }
    else if (id == WATER_RIGHT)
    {
        object->collisionRect.x = object->position.x;
        object->collisionRect.y = object->position.y;
        object->collisionRect.w = 39;
        object->collisionRect.h = 64;
    }
    else if (id == WATER_BOTTOM_LEFT)
    {
        object->collisionRect.x = object->position.x+25;
        object->collisionRect.y = object->position.y;
        object->collisionRect.w = 39;
        object->collisionRect.h = 44;
    }
    else if (id == WATER_BOTTOM)
    {
        object->collisionRect.x = object->position.x;
        object->collisionRect.y = object->position.y;
        object->collisionRect.w = 64;
        object->collisionRect.h = 44;
    }
    else if (id == WATER_BOTTOM_RIGHT)
    {
        object->collisionRect.x = object->position.x;
        object->collisionRect.y = object->position.y;
        object->collisionRect.w = 39;
        object->collisionRect.h = 44;
    }
    else if (id == BUSH3)
    {
        object->collisionRect.x = object->position.x+20;
        object->collisionRect.y = object->position.y+8;
        object->collisionRect.w = 24;
        object->collisionRect.h = 44;
    }
    else if (id == BUSH2)
    {
        object->collisionRect.x = object->position.x+9;
        object->collisionRect.y = object->position.y+8;
        object->collisionRect.w = 44;
        object->collisionRect.h = 50;
    }
    else if (id == TRUNK1)
    {
        object->collisionRect.x = object->position.x+3;
        object->collisionRect.y = object->position.y+21;
        object->collisionRect.w = 52;
        object->collisionRect.h = 20;
    }
    else if (id == TRUNK2)
    {
        object->collisionRect.x = object->position.x+23;
        object->collisionRect.y = object->position.y+27;
        object->collisionRect.w = 20;
        object->collisionRect.h = 20;
    }
    else if (id == PUBBLES1)
    {
        object->collisionRect.x = object->position.x+6;
        object->collisionRect.y = object->position.y+8;
        object->collisionRect.w = 52;
        object->collisionRect.h = 39;
    }
    else if (id == TREE1_BOTTOM_LEFT)
    {
        object->collisionRect.x = object->position.x+55;
        object->collisionRect.y = object->position.y;
        object->collisionRect.w = 8;
        object->collisionRect.h = 27;
    }
    else if (id == TREE1_BOTTOM_RIGHT)
    {
        object->collisionRect.x = object->position.x;
        object->collisionRect.y = object->position.y;
        object->collisionRect.w = 16;
        object->collisionRect.h = 27;
    }
    else if (id == TREE2_BOTTOM_RIGHT)
    {
        object->collisionRect.x = object->position.x+51;
        object->collisionRect.y = object->position.y;
        object->collisionRect.w = 11;
        object->collisionRect.h = 23;
    }
    else if (id == TREE2_BOTTOM_LEFT)
    {
        object->collisionRect.x = object->position.x;
        object->collisionRect.y = object->position.y;
        object->collisionRect.w = 8;
        object->collisionRect.h = 23;
    }

}

void drawobject(Object object, Position worldpos, SDL_Surface *texture, SDL_Surface *screen)
{
    SDL_Rect src = { 192, 64, 64, 64 };
    SDL_Rect dest = { 0, 0, 64, 64 };

    src.x = object.sheetPos.x;
    src.y = object.sheetPos.y;
    src.w = object.sheetPos.w;
    src.h = object.sheetPos.h;
    dest.x = object.position.x-worldpos.x;
    dest.y = object.position.y-worldpos.y;
    dest.w = src.w;
    dest.h = src.h;

    SDL_BlitSurface(texture, &src, screen, &dest);

    #ifdef _SHOWCOLLISIONRECTS
        rectangleColor(screen, object.collisionRect.x-worldpos.x, object.collisionRect.y-worldpos.y, object.collisionRect.x-worldpos.x+object.collisionRect.w, object.collisionRect.y-worldpos.y+object.collisionRect.h, 0xff0000ff);
    #endif // _SHOWCOLLISIONRECTS
}
