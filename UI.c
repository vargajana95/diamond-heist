#include <SDL.h>
#include <SDL_image.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_rotozoom.h>
#include <SDL_ttf.h>
#include <string.h>

#include "UI.h"

TTF_Font *font_medium;
TTF_Font *font_large;

MENUList menu;
MENUList ingameMenu;
MENUList endMenu;

int addMENUElem(MENUList *menu, char *text, Uint32 backgroundcolor, int x, int y, int w, int h, State onclick, fontType ftype)
{
    MENUElem *elem;

    elem = (MENUElem*) malloc(sizeof(MENUElem));
    elem->text = text;
    elem->backgroundcolor = backgroundcolor;
    elem->rect.x = x;
    elem->rect.y = y;
    elem->rect.w = w;
    elem->rect.h = h;
    elem->onclick = onclick;

    elem->kov = menu->elso->kov;
    elem->elozo = menu->elso;
    menu->elso->kov->elozo = elem;
    menu->elso->kov = elem;

    return y+h+1; //az alatta l�v� men�elem y koorin�t�ja ez legyen
}
void registerFonts()
{
    TTF_Init();
    font_medium = TTF_OpenFont("impact.ttf", 24);
    if (!font_medium) {
        fprintf(stderr, "Nem sikerult megnyitni a fontot! %s\n", TTF_GetError());
        exit(5);
    }
    font_large = TTF_OpenFont("impact.ttf", 40);
    if (!font_large) {
        fprintf(stderr, "Nem sikerult megnyitni a fontot! %s\n", TTF_GetError());
        exit(5);
    }
}
void registerMenus()
{
    menu = makeMenu();
    ingameMenu = makeMenu();
    endMenu = makeMenu();

    int y;
    y = addMENUElem(&menu, "New Game", 0x616161ff, 400, 200, 150, 50, INIT, FONT_MEDIUM);
    y = addMENUElem(&menu, "Load Game", 0x616161ff, 400, y, 150, 50, LOAD, FONT_MEDIUM);
    y = addMENUElem(&menu, "Quit", 0x616161ff, 400, y, 150, 50, QUIT, FONT_MEDIUM);

    //Ingame menu
    y = addMENUElem(&ingameMenu, "Resume", 0x616161ff, 10, 200, 150, 50, GAME, FONT_MEDIUM);
    y = addMENUElem(&ingameMenu, "Main Menu", 0x616161ff, 10, y, 150, 50, MENU, FONT_MEDIUM);
    y = addMENUElem(&ingameMenu, "Save Game", 0x616161ff, 10, y, 150, 50, SAVE, FONT_MEDIUM);
    y = addMENUElem(&ingameMenu, "Load Game", 0x616161ff, 10, y, 150, 50, LOAD, FONT_MEDIUM);
    y = addMENUElem(&ingameMenu, "Quit", 0x616161ff, 10, y, 150, 50, QUIT, FONT_MEDIUM);

    //EndGame menu
    y = addMENUElem(&endMenu, "Main Menu", 0x616161ff, 280, 300, 150, 50, MENU, FONT_MEDIUM);
    y = addMENUElem(&endMenu, "New Game", 0x616161ff, 280, y, 150, 50, INIT, FONT_MEDIUM);
    y = addMENUElem(&endMenu, "Load Game", 0x616161ff, 280, y, 150, 50, LOAD, FONT_MEDIUM);
    y = addMENUElem(&endMenu, "Quit", 0x616161ff, 280, y, 150, 50, QUIT, FONT_MEDIUM);
}

void closeFonts()
{
    TTF_CloseFont(font_medium);
    TTF_CloseFont(font_large);
}
void inizializeMenus()
{
    registerFonts();
    registerMenus();
}
void drawMenus(SDL_Surface *screen)
{
    if(GameState == MENU)
        drawMenu(screen, &menu);
    else if (GameState == PAUSED)
        drawIngameMenu(screen, &ingameMenu);
    else if(GameState == END || GameState == WIN)
        drawEndScreen(screen, &endMenu);



}
void printText(char * string, SDL_Surface *screen, int x, int y, fontType ftype, SDL_Color color)
{
    SDL_Rect hova = {x, y, 0, 0};
    TTF_Font *font;
    switch(ftype)
    {
        case FONT_LARGE:   font = font_large;
                            break;
        default:            font = font_medium;
                            break;
    }
    SDL_Surface *text =  TTF_RenderUTF8_Blended(font, string, color);

    SDL_BlitSurface(text, NULL, screen, &hova);
    SDL_FreeSurface(text);
}
void drawUI(World world, SDL_Surface *screen)
{
    SDL_Color white = {255, 255, 255};


    //Kirajzoljuk a Health bar-t
    int x1, y1, x2, y2, margin, w, h, length;
    margin = 2;
    h = 10+2*margin;
    x1 = 10;
    y1 = 10;
    length = 200;
    w = length+2*margin;
    x2 = x1+w;
    y2 = y1+h;
    rectangleColor(screen, x1, y1, x2, y2, 0xffffffff);
    if(world.player.health >0)
        boxColor(screen, x1+margin, y1+margin,
             x1+(double)world.player.health/world.player.maxHealth*(w-margin), y2-margin, 0xff0000ff);

    //Kirajzoljuk a stamina Bar-t
    rectangleColor(screen, x1, y1+h+5, x2, y2+h+5, 0xffffffff);
    if(world.player.stamina >0)
        boxColor(screen, x1+margin, y1+margin+h+5,
             x1+(double)world.player.stamina/MAX_STAMINA*(w-margin), y2-margin+h+5, 0x00cc44ff);


    //Kirajzoljuk, hogy h�ny gy�m�ntot vett�nk eddig fel
    double scale = 0.6;
    SDL_Rect src = {64*scale, 0, 64*scale, 64*scale};
    SDL_Rect dest = {SCREEN_WIDTH-150, 10, 0, 0};
    SDL_Surface *diamond = rotozoomSurface(DiamondIMG, 0, scale, 0);
    char string[50];

    SDL_BlitSurface(diamond, &src, screen, &dest);

    sprintf(string, "X %02d/%02d", world.player.diamonds, world.maxDiamonds);
    printText(string, screen, SCREEN_WIDTH-100, 15, FONT_MEDIUM, white);
}

void DrawMENUElem(SDL_Surface *screen, MENUElem elem)
{
    Rect mouse;
    SDL_GetMouseState(&mouse.x, &mouse.y);
    mouse.w = 0;
    mouse.h = 0;
    SDL_Color white = {255, 255, 255};


    if(isCollidingWith(mouse, elem.rect))
    {
        //az eg�r az men�elemen van -> kirajzoljuk a htteret
        boxColor(screen, elem.rect.x, elem.rect.y, elem.rect.x+elem.rect.w, elem.rect.y+elem.rect.h, elem.backgroundcolor);
        if(SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
            //TODO: Lehet hogy nem itt kellene lennie ->MENU update
            GameState = elem.onclick;
    }
    printText(elem.text, screen, elem.rect.x, elem.rect.y, elem.ftype, white);


}
MENUList makeMenu()
{
    MENUList menu;
    int y;
    menu.elso = (MENUElem*) malloc(sizeof(MENUElem));
    menu.utolso = (MENUElem*) malloc(sizeof(MENUElem));
    menu.elso->kov = menu.utolso;
    menu.utolso->elozo = menu.elso;



    return menu;
}
void drawMenu(SDL_Surface *screen, MENUList *menu)
{
    SDL_Color white = {255, 255, 255};
    MENUElem *aktelem;

    boxColor(screen, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0x000000FF);

    for(aktelem = menu->elso->kov; aktelem != menu->utolso; aktelem = aktelem->kov)
    {
        DrawMENUElem(screen, *aktelem);

    }

}
void drawIngameMenu(SDL_Surface *screen, MENUList *menu)
{
    MENUElem *aktelem;

    boxColor(screen, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0x00000080);
    boxColor(screen, 0, 0, 200, SCREEN_HEIGHT, 0x00000096);

    for(aktelem = menu->elso->kov; aktelem != menu->utolso; aktelem = aktelem->kov)
    {
        DrawMENUElem(screen, *aktelem);
    }
}
void drawEndScreen(SDL_Surface *screen, MENUList *menu)
{
    SDL_Color white = {255, 255, 255};
    MENUElem *aktelem;

    //els�t�t�tj�k a k�pernyot, �s ki�rjuk hogy game over
    boxColor(screen, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0x00000080);
    if(GameState == END)
        printText("Game Over", screen, SCREEN_WIDTH/2-80, SCREEN_HEIGHT/2-50, FONT_LARGE, white);
    else if(GameState == WIN)
    {
        printText("Congratulations!", screen, SCREEN_WIDTH/2-100, SCREEN_HEIGHT/2-150, FONT_LARGE, white);
        printText("You have collected all the diamonds", screen, SCREEN_WIDTH/2-300, SCREEN_HEIGHT/2-100, FONT_LARGE, white);

    }

    for(aktelem = menu->elso->kov; aktelem != menu->utolso; aktelem = aktelem->kov)
    {
        DrawMENUElem(screen, *aktelem);

    }
}


