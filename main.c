#include <SDL.h>
#include <SDL_image.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_rotozoom.h>
#include <SDL_ttf.h>
#include <math.h>
#include <time.h>


#include "world.h"
#include "main.h"
#include "UI.h"



static Uint32 idozit(Uint32 ms, void* param) {
    SDL_Event ev;
    ev.type = SDL_USEREVENT;
    SDL_PushEvent(&ev);
    return ms;
}
static Uint32 idozit2(Uint32 ms, void* param) {
    SDL_Event ev;
    ev.type = SDL_USEREVENT+1;
    SDL_PushEvent(&ev);
    return ms;
}


void move(World *world)
{
    Uint8 *keypressed;
    keypressed = SDL_GetKeyState(NULL);
    Position playerScreenPos;
    Position nextpos = {0, 0};
    Rect collisionrect;
    Character *player = &world->player;
    static int isBowCharging = 0; //TODO: Nem kellene static-nak lennie. Talan a character strukturaba kellene tenni
    static int isBowShooting = 0;
    double speed = player->speed;
        //int isoutofmap = isOutOfMap(player->position, world);

    playerScreenPos.x = player->position.x - world->min.x;
    playerScreenPos.y = player->position.y - world->min.y;

    collisionrect = player->collisionRect;

    if(keypressed[SDLK_LSHIFT] && (keypressed[SDLK_DOWN] || keypressed[SDLK_UP]
                                   || keypressed[SDLK_RIGHT] || keypressed[SDLK_LEFT]))
    {
        if(player->stamina >0)
        {
            speed += 3;
            player->stamina -= 1;
        }
        else
            player->stamina = 0;

    }
    else
    {
        if(player->stamina < MAX_STAMINA)
            player->stamina+=1;
        else
            player->stamina= MAX_STAMINA;
    }

    if(keypressed[SDLK_DOWN])
    {
        updateFrames(player, 1);
        player->direction = DOWN1;
        player->moveid = DOWN;
        if (player->position.y >= world->max.y-64)
            player->position.y = world->max.y-64;
        else
            nextpos.y += speed;

        if(playerScreenPos.y >= (SCREEN_HEIGHT-64)/2)
        {
            if(world->min.y >= world->max.y-SCREEN_HEIGHT)
                world->min.y = world->max.y-SCREEN_HEIGHT;
            else
                world->min.y +=speed;
        }
    }
    else if(keypressed[SDLK_UP])
    {
        updateFrames(player, 1);
        player->direction = UP1;
        player->moveid = UP;
        if (player->position.y <= 0)
            player->position.y = 0;
        else
            nextpos.y -= speed;
        if(playerScreenPos.y <= (SCREEN_HEIGHT-64)/2)
        {
            if(world->min.y <= 0)
                world->min.y = 0;
            else
                world->min.y -=speed;
        }
    }
    else if(keypressed[SDLK_LEFT])
    {
        updateFrames(player, 1);
        player->direction = LEFT1;
        player->moveid = LEFT;
        if (player->position.x <= 0)
            player->position.x = 0;
        else
            nextpos.x -= speed;
        if(playerScreenPos.x <= (SCREEN_WIDTH-64)/2)
        {
            if(world->min.x<= 0)
                world->min.x = 0;
            else
                world->min.x -=speed;
        }
    }
    else if(keypressed[SDLK_RIGHT])
    {
        updateFrames(player, 1);
        player->direction = RIGHT1;
        player->moveid = RIGHT;
        if (player->position.x >= world->max.x-64)
            player->position.x = world->max.x-64;
        else
            nextpos.x += speed;
        if(playerScreenPos.x >= (SCREEN_WIDTH-64)/2)
        {
            if(world->min.x >= world->max.x-SCREEN_WIDTH)
                world->min.x = world->max.x-SCREEN_WIDTH;
            else
                world->min.x +=speed;
        }
    }

    else if (keypressed[SDLK_SPACE])
    {
        if(player->moveid == UP)
            player->moveid = SHOOT_UP;
        else if(player->moveid == LEFT)
            player->moveid = SHOOT_LEFT;
        else if(player->moveid == RIGHT)
            player->moveid = SHOOT_RIGHT;
        else if(player->moveid == DOWN)
            player->moveid = SHOOT_DOWN;

        if(player->moveid == LET_SHOOT_UP)
            player->moveid = SHOOT_UP;
        else if(player->moveid == LET_SHOOT_LEFT)
            player->moveid = SHOOT_LEFT;
        else if(player->moveid == LET_SHOOT_RIGHT)
            player->moveid = SHOOT_RIGHT;
        else if(player->moveid == LET_SHOOT_DOWN)
            player->moveid = SHOOT_DOWN;

        if(updateFrames(player, 0)) //veget ert az animacio, az ijj fel van huzva
        {
            isBowCharging = 1;
            isBowShooting = 0;
            //player->currentFrame = 8;
        }
    }
    else if(!keypressed[SDLK_SPACE] && isBowCharging)
    {
        //player->currentFrame = 0;
        if(!isBowShooting)
        {
            isBowShooting = 1;
            addArrow(world, player->position, player->direction);
            player->currentFrame = 0;
        }

        if(player->moveid == SHOOT_UP)
            player->moveid = LET_SHOOT_UP;
        else if(player->moveid == SHOOT_LEFT)
            player->moveid = LET_SHOOT_LEFT;
        else if(player->moveid == SHOOT_RIGHT)
            player->moveid = LET_SHOOT_RIGHT;
        else if(player->moveid == SHOOT_DOWN)
            player->moveid = LET_SHOOT_DOWN;

        //if(player->currentFrame == player->movetype[player->moveid].numberOfFrames-1)
        if(updateFrames(player, 0))
        {
            //player->currentFrame = 0;
            isBowCharging = 0;
            isBowShooting = 0;
            if(player->moveid == LET_SHOOT_UP)
                player->moveid = SHOOT_UP;
            if(player->moveid == LET_SHOOT_LEFT)
                player->moveid = SHOOT_LEFT;
            if(player->moveid == LET_SHOOT_RIGHT)
                player->moveid = SHOOT_RIGHT;
            if(player->moveid == LET_SHOOT_DOWN)
                player->moveid = SHOOT_DOWN;

            player->currentFrame = 0;
                //updateFrames(player, 0);
        }
    }
    else
    {
        //updateFrames(player, 0);
        //isBowCharging = 0;
        //player->moveid = NONE;
        player->currentFrame = 0;
    }


    collisionrect.x += nextpos.x;
    collisionrect.y += nextpos.y;

    if(!isColliding(world, collisionrect))
    {
        player->position.x += nextpos.x;
        player->position.y += nextpos.y;
    }
    //megn�zz�k, hogy a player nem ment r� egy itemre
    checkForItemPickups(world);

    if(player->diamonds >= world->maxDiamonds)
    {
        //Megnyert�k a j�t�kot
        GameState = WIN;
    }



}


int main(int argc, char *argv[]) {
    SDL_Event event;


    int i, j;
    int init = 0;
    World world = {0};

    SDL_TimerID id, id2;

    srand(time(NULL));
    /* SDL inicializ�l�sa �s ablak megnyit�sa */
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);
    screen=SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 0, SDL_ANYFORMAT);
    if (!screen) {
        fprintf(stderr, "Nem sikerult megnyitni az ablakot!\n");
        exit(1);
    }
    SDL_WM_SetCaption("SDL peldaprogram", "SDL peldaprogram");


    id = SDL_AddTimer(30, idozit, NULL);
    //id2= SDL_AddTimer(100, idozit2, NULL);
    world.player.IMG = IMG_Load("player3.png");
    if (!world.player.IMG) {
        fprintf(stderr, "Nem sikerult betolteni a kepfajlt!\n");
        exit(2);
    }

    DiamondIMG = IMG_Load("diamond.png");
    EnemyIMG = IMG_Load("enemy.png");
    Enemy2IMG = IMG_Load("enemy2.png");
    ArrowIMG = IMG_Load("arrow.png");

    inizializeMenus();

    world.texture = IMG_Load("map.png");



    GameState = MENU;

    while (GameState != QUIT) {
        SDL_WaitEvent(&event);

        switch (event.type)
        {
            case SDL_USEREVENT:

                switch (GameState)
                {
                    case MENU:      drawMenus(screen);
                                    break;
                    case PAUSED:    draw(&world, screen);
                                    drawUI(world, screen);
                                    break;

                    case GAME:      updateCharacters(&world);
                                    draw(&world, screen);
                                    drawUI(world, screen);
                                    break;
                    case INIT:      if(init)
                                    {
                                        //csak akkor t�r�lj�k, ha l�teznek
                                        freeArrowList(&world.arrows);
                                        freeItemList(&world.items);
                                        freeEnemyList(&world.enemies);
                                        init = 0;
                                    }
                                    init = initializeWorld(&world);
                                    GameState = GAME;
                                    break;
                    case SAVE:      SaveGame(&world);
                                    GameState = PAUSED;
                                    break;
                    case LOAD:      if(init)
                                    {
                                        //csak akkor t�r�lj�k, ha l�teznek
                                        freeArrowList(&world.arrows);
                                        freeItemList(&world.items);
                                        freeEnemyList(&world.enemies);
                                        init = 0;
                                    }
                                    inizializeLists(&world);
                                    loadWorld(&world);
                                    init = 1;
                                    if(LoadGame(&world))
                                        GameState = GAME;
                                    else
                                        GameState = MENU;
                                    break;

                    case END:       updateCharacters(&world);
                                    draw(&world, screen);
                                    break;
                    case WIN:       //updateCharacters(&world);
                                    draw(&world, screen);
                                    break;
                }
            case SDL_KEYDOWN:
                if(event.key.keysym.sym == SDLK_ESCAPE)
                    if(GameState == GAME)
                        GameState = PAUSED;
                    else if (GameState == PAUSED)
                        GameState = GAME;




                break;
            case SDL_QUIT:
                GameState = QUIT;
                break;
        }
        SDL_Flip(screen);


    }

    //TODO: list�k felszabad�t�sa, k�pek felszabad�t�sa
    /* nincs mar ra szukseg: felszabaditjuk a memoriat */
    SDL_RemoveTimer(id);
    SDL_FreeSurface(screen);
    SDL_FreeSurface(world.player.IMG);
    SDL_FreeSurface(DiamondIMG);
    SDL_FreeSurface(ArrowIMG);
    SDL_FreeSurface(EnemyIMG);
    closeFonts();
    if(init)
    {
        //csak akkor t�r�lj�k, ha l�teznek
        freeArrowList(&world.arrows);
        freeItemList(&world.items);
        freeEnemyList(&world.enemies);
    }

    /* ablak bezarasa */
    SDL_Quit();

    return 0;
}
