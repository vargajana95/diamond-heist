#ifndef WORLD_H_INCLUDED
#define WORLD_H_INCLUDED

//#define _SHOWCOLLISIONRECTS //Teszt uzemmod, mutatja a collisionRectek vonalat

#include "main.h"
#include "object.h"
#include "item.h"
#include "arrow.h"
#include "character.h"


#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 512
#define SHEET_SIZE 768
#define BLOCK_SIZE 64
#define MAX_HEALTH 100
#define MAX_STAMINA 100





typedef enum State
{
    INIT,
    MENU,
    PAUSED,
    LOAD,
    GAME,
    QUIT,
    SAVE,
    WIN,
    END
} State;
typedef struct World {
    Object **object;
    Object **objectBottom; //az �tl�tsz� k�pek al� kell ez
    ArrowList arrows;
    EnemyList enemies;
    ItemList items;
    Character player;
    Position min;
    Position max;
    SDL_Surface *texture;
    int numberOfDiamonds;
    int numberOfEnemies;
    int maxDiamonds;

} World;

State GameState;

int isCollidingWith(Rect r1, Rect r2);
int isColliding(World *world, Rect rect);
int isOutOfMap(World *world, Position position);
int isOutOfScreen(World *world, Position position);
void drawScreen(World *world, SDL_Surface *screen, Layer layer);
void draw(World *world, SDL_Surface *screen);
void loadWorld(World *world);
int initializeWorld(World *world);



void move(World *world);
int SaveGame(World *world);
int LoadGame(World *world);
void inizializeLists(World *world);





#endif // WORLD_H_INCLUDED
