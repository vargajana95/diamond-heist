#ifndef UI_H_INCLUDED
#define UI_H_INCLUDED

#include "main.h"
#include "world.h"

typedef enum fontType {
    //FONT_SMALL,
    FONT_MEDIUM,
    FONT_LARGE
} fontType;
typedef struct MENUElem
{
    char *text;
    Uint32 backgroundcolor;
    Rect rect;
    State onclick;
    fontType ftype;
    struct MENUElem *kov, *elozo;

} MENUElem;
typedef struct MENUList
{
    MENUElem *elso;
    MENUElem *utolso;
} MENUList;

void drawUI(World world, SDL_Surface *screen);
void printText(char * string, SDL_Surface *screen, int x, int y, fontType ftype, SDL_Color color);

void drawMenu(SDL_Surface *screen, MENUList *menu);
int addMENUElem(MENUList *menu, char *text, Uint32 backgroundcolor, int x, int y, int w, int h, State onclick, fontType ftype);
void DrawMENUElem(SDL_Surface *screen, MENUElem elem);
MENUList makeMenu();
void drawIngameMenu(SDL_Surface *screen, MENUList *menu);
void drawEndScreen(SDL_Surface *screen, MENUList *menu);
void registerFonts();
void closeFonts();
void drawMenus(SDL_Surface *screen);
void inizializeMenus();


#endif // UI_H_INCLUDED
