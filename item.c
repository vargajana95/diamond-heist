#include "item.h"
#include "world.h"
#include <SDL_rotozoom.h>



void AddItem(World *world, Position position, int numberOfFrames, ItemID id)
{
    ItemListaElem *newItem;
    newItem = (ItemListaElem*) malloc(sizeof(ItemListaElem));

    newItem->item.position = position;
    newItem->item.numberOfFrames = numberOfFrames;
    newItem->item.currentFrame = 0;
    newItem->item.id = id;
    if(id == DIAMOND)
    {

        Rect collisionRect = {24, 24, 16, 16};

        newItem->item.IMG = DiamondIMG;
        newItem->item.collisionRect = collisionRect;
        newItem->item.collisionRect.x = position.x+collisionRect.x;
        newItem->item.collisionRect.y = position.y+collisionRect.y;
    }

    newItem->kov = world->items.elso->kov;
    newItem->elozo = world->items.elso;
    world->items.elso->kov->elozo = newItem;
    world->items.elso->kov = newItem;
}
void removeItem(ItemListaElem *item)
{
    //"kifuzz�k" a listabol
    item->elozo->kov =item->kov;
    item->kov->elozo = item->elozo;
    //SDL_FreeSurface(item->item.IMG); //Nem kellen mindig ezt felszabaditani, egy globalis valtozoba kellene tenni a kepet.
    free(item);
}
void drawItems(World *world, SDL_Surface *screen, Item item)
{
    double scale = 0.75;
    SDL_Rect src = { item.currentFrame*BLOCK_SIZE*scale, 0, BLOCK_SIZE*scale, BLOCK_SIZE*scale };
    SDL_Rect dest = { item.position.x-world->min.x+(BLOCK_SIZE-BLOCK_SIZE*scale)/2, item.position.y-world->min.y+(BLOCK_SIZE-BLOCK_SIZE*scale)/2, BLOCK_SIZE*scale, BLOCK_SIZE*scale };
    SDL_BlitSurface(rotozoomSurface(item.IMG, 0, scale, 0), &src, screen, &dest);

    #ifdef _SHOWCOLLISIONRECTS
        rectangleColor(screen, item.collisionRect.x-world->min.x, item.collisionRect.y-world->min.y, item.collisionRect.x-world->min.x+item.collisionRect.w, item.collisionRect.y-world->min.y+item.collisionRect.h, 0xff0000ff);
    #endif // _SHOWCOLLISIONRECTS

}
void freeItemList(ItemList *lista)
{
    ItemListaElem *mozgo;

    mozgo = lista->elso->kov;
    while( mozgo != lista->utolso)
    {
        ItemListaElem *temp = mozgo->kov;
        free(mozgo);
        mozgo = temp;
    }
    //felszabad�tjuk a str�zs�t is
    free(lista->elso);
    free(lista->utolso);
}
void checkForItemPickups(World *world)
{
    ItemListaElem *currentItem;

    currentItem = world->items.elso->kov;
    while(currentItem != world->items.utolso)
    {
        //TODO Csak a kepernyon levokkel kellene elv�gezni a cllusionchecket
            //Talan a world->object classnak kellene egy items adattag. --> az egeszet atirni

        ItemListaElem *temp = currentItem->kov;
        if(isCollidingWith(world->player.collisionRect, currentItem->item.collisionRect))
        {
            //felvesszuk az adott itemet
            removeItem(currentItem);
            world->player.diamonds++;
        }
        currentItem = temp;
    }
}


int updateItemFrames(Item *item, int repeat) //1-el ter vissza, ha az animacio veget ert
{
    if(item->currentFrame >= item->numberOfFrames-1)
       if(repeat) item->currentFrame = 0;
       else return 1;
    else
    {
        item->currentFrame +=1;
        return 0;
    }
}
void generateDiamonds(World *world)
{
    int x, y;
    int i;

    for(i=0; i < world->maxDiamonds-world->numberOfDiamonds; i++)
    {
        x = rand()%(world->max.x/BLOCK_SIZE );
        y = rand()%(world->max.y/BLOCK_SIZE );


        while(world->object[y][x].type != WALKABLE)
        {
            //am�g van valami az adott helyen, addig m�s helyet kell gener�lni
            x = rand()%(world->max.x/BLOCK_SIZE );
            y = rand()%(world->max.y/BLOCK_SIZE );
        }
        Position position;
        position.x = x*BLOCK_SIZE;
        position.y = y*BLOCK_SIZE;
        AddItem(world, position, 8, DIAMOND);
    }
}
